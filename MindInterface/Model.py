from MindInterface.config import *
from math import *
import json

UNKNOWN, BALL, POLE, ROBOT = "unkown", "ball", "pole", "robot"
x, y = 0, 1
WALK = 0

class BaseObject(object):
    """This class defines what attributes a basic object should have. This is 
    the objects the robot can detect."""
    def __init__(self, pos, kind = UNKNOWN):
        """Initializes the object with a position and a kind.
        Position is a tuple of (r,a).
        Kind is one of the strings ["ball", "pole", "robot", "unkown"]."""
        self.position = pos
        self.kind = kind

    def getJSON(self):
        """Returns the JSON object of this object."""
        return json.dumps(self, default=lambda o: o.__dict__)

class World:
    """This class stores a list of objects detected by the robot. This list is 
    refreshed everytime the simulation is updated (everytime the robot receives 
    information about the world)"""
    def __init__(self):
        """Initializes the `objects_list` attribute as an empty list"""
        self.objects_list = []

    def getJSON(self):
        """Returns the JSON object of this object"""
        return json.dumps(self, default=lambda o: o.__dict__)
        #, sort_keys=True, indent=4)

    def getDict(self):
        """Returns the list as a python dictionary"""
        return json.loads(self.toJSON())
