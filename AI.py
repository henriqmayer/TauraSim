# these two imports are essential
from MindInterface import Simulation # imports the simulation
from MindInterface.config import *   # imports configuration for connection and stuff

import time

# starts the simulation and stores a reference to the robot you are controlling
robot = Simulation.start()


# cycle where you update the simulation
while robot.updateSimulation():
    # you can access the objects detected in the world directly through the 
    # attribute robot.world but I prefer using the robot.getWorld() method
    world = robot.perceiveWorld()
    # if no world was received there was a problem in the connection
    if not world:
        sys.exit("No world received")
    # available commands are: 
    # 1. robot.setMovementVector(mv)
    # where mv is Point2(radius, alpha, phi)
    # with radius from 0 to 1
    # 2. robot.setKick(k)
    # where k is 1 for left leg kick, -1 for right leg kick and 0 for no kick
    # 3. there's no 3
    # now, do the magic!


    # I don't know where ball is
    ball = None
    for obj in world.objects_list:
        # if some object has kind ball
        if obj.kind == "ball":
            # that object is the ball!
            ball = obj

    # if I have ball
    if ball:
        # and it is far
        if ball.position.r > 29:
            # go to ball facing ball
            robot.setMovementVector( Point2(r=1, a=ball.position.a, 
                phi=ball.position.a) )
        # if close to ball
        else:
            # stop and kick it!
            robot.setMovementVector( Point2() )
            robot.setKick( 1 )

    # a sleep on the simulation so we can see what's happening
    time.sleep(1/10)

